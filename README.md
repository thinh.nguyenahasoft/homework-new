# homework-new

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Config

- vue add router
- npm install sass-loader node-sass style-loader
- npm install bootstrap@4.6.0 bootstrap-vue@2.21.2

# Ignore

- :head-variant="headVariant"
- :table-variant="tableVariant"
- https://stackoverflow.com/questions/45179061/file-input-on-change-in-vue-js
- https://stackoverflow.com/questions/49106045/preview-an-image-before-it-is-uploaded-vuejs

# Bug Form đăng nhập

- Nhiều khoảng trắng vẫn cho đăng nhập
- Không giới hạn số ký tự.


# Old code

```
async demoCallApi() {
      var res = await axios.post(
        "https://ahasoft-salon-admin-http-aggregator-dev.azurewebsites.net/api/aggr/v1/Client/GetClient",
        {
          clientId: 892487,
          shopId: 600893,
        },
        {
          headers: {
            Authorization: "Bearer " + sessionStorage.getItem("token"),
          },
        }
      );
      console.log("Demo api = ", res);
    },
```

# Fix bug HW2

- Đã fix max length.
- Đã fix thiếu phone number.
- Đã fix export thiếu cột note.
