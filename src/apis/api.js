
import axios from "axios";
var getHeader = (moreHeader) => {
    if (moreHeader){
        return {
            headers: {
                Authorization: "Bearer " + sessionStorage.getItem("token"), ...moreHeader
            }
        }
    }
    return {
        headers: {
            Authorization: "Bearer " + sessionStorage.getItem("token")
        }
    }
}

export const getAPIs = async (url, payload, moreHeader = null) => {
    return await axios.get(url, payload, getHeader(moreHeader));
}

export const postAPIs = async (url, payload, moreHeader = null, needHeader = true) => {
    if (needHeader) {
        return await axios.post(url, payload, getHeader(moreHeader));
    }
    else {
        return await axios.post(url, payload);
    }
}

export const putAPIs = async (url, payload, moreHeader = null) => {
    return await axios.put(url, payload, getHeader(moreHeader));
}

export const deleteAPIs = async (url, payload, moreHeader = null) => {
    return await axios.delete(url, {...getHeader(moreHeader), data: payload });
}