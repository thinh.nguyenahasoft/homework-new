const BASE_URL_API = "https://ahasoft-salon-admin-http-aggregator-dev.azurewebsites.net/api";
export const CLIENT_API = `${BASE_URL_API}/read/v1/clients/Client/Active`;
export const LOGIN_API = `${BASE_URL_API}/aggr/v1/auth/Login/Subscriber?culture=en-US&ui-culture=en-US`;
export const SPECIFIC_CILENT_API =  `${BASE_URL_API}/aggr/v1/Client/GetClient`;
export const SET_IMAGE_API = `${BASE_URL_API}/cmd/v1/clients/ClientImage`;
export const CLIENT_GROUP_API = `${BASE_URL_API}/read/v1/clients/ClientCodeSetup/ClientGroup/List`;
export const EDIT_CLIENT_INFO_API = `${BASE_URL_API}/aggr/v1/Client/EditClient`;
export const DELETE_CLIENT_API = `${BASE_URL_API}/aggr/v1/Client/UpdateClientToDeleted`;
export const ADD_CLIENT_API = `${BASE_URL_API}/aggr/v1/Client/CreateClient`;
export const NEXT_MEMBER_NUMBER_API = `${BASE_URL_API}/read/v1/clients/Client/NextMemberNumber`;
export const AVATAR_URL = (shopId, imageName) => `https://ahasoftsaloncommondev.blob.core.windows.net/clients/VN/client-img/${shopId}/${imageName}`;
export const DELETE_IMAGE_API = `${BASE_URL_API}/cmd/v1/clients/ClientImage`;

export const MONTH = {
    1: 'Jan',
    2: 'Feb',
    3: 'Mar',
    4: 'Apr',
    5: 'May',
    6: 'Jun',
    7: 'Jul',
    8: 'Aug',
    9: 'Sep',
    10: 'Oct',
    11: 'Nov',
    12: 'Dec'
}