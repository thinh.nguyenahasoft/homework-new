import Vue from 'vue'
import VueRouter from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import LoginForm from '../views/LoginForm/LoginForm.vue'
import ClientPage from '../views/ClientPage/ClientPage.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: LoginForm
  },
  {
    path: '/client',
    name: 'client',
    component: ClientPage
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
